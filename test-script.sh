#! /bin/bash
set -e
SUDO=""
if [ "$EUID" -ne 0 ]
then echo "Using sudo"
  SUDO="sudo"
fi

pip --version
pip install -U -r requirements.txt
pip install -U -r test-requirements.txt
# pyenv version
# replace LIBRARY_NAME by your library name
for file in *; do
    if [ -d $file ] && [ $file != "build" ] && [ $file != "dist" ] && [ $file != "docs" ] && [ $file != *"egg-info"* ] && [ $file != "test" ]; then
	pylint $file -f parseable -r n --disable=superfluous-parens,invalid-name --max-line-length=120
	pycodestyle $file --max-line-length=120 --ignore=E402
	pydocstyle $file --ignore=D204,D213,D406,D407,D203
	tox $file
    fi
done
tox --version
coverage report
