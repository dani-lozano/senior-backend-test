# API Template
To serve as an example on how to create new APIs.

Prerequisites:
 - Install Python 3.6
 - Install a local MongoDB.
 - Create a users collection in db local.
 - Install the project dependencies in requirements.txt.

 ![infraestructure](wide-eyes.png)

 End point added

 - /api (POST)

 You can call to /api endpoint with the following json body parameters:

 - email
 - password
 - city (only valid cities)

 Local testing:

 - run test-script.sh

 Integration test:

 - run docker-compose up
 - run docker-compose build
 - open your browser and call for http://192.168.99.100/status (or the ip obtained for the docker container)

 Deployment test:

 - Execute the bitbucket pipeline and see the results :)

 ![pipeline result](bitbucket-pipeline.png)

