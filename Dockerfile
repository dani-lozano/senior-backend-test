FROM python:3.6
ADD . /usr/src/app
WORKDIR /usr/src/app
RUN pip install -U -r requirements.txt
RUN pip install -U -r test-requirements.txt
CMD ["python", "api/router.py"]