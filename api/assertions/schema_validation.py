"""Jsonschema validation helper."""

import json
from os.path import join, dirname
from jsonschema import validate, exceptions, FormatChecker


def assert_valid_schema(data, schema_file):
    """Check whether the given data matches the schema."""
    schema = _load_json_schema(schema_file)
    try:
        validate(data, schema, format_checker=FormatChecker())
        return True
    except (exceptions.ValidationError, exceptions.SchemaError):
        return False


def _load_json_schema(filename):
    """Load the given schema file."""
    relative_path = join('schemas', filename)
    absolute_path = join(dirname(__file__), relative_path)

    with open(absolute_path) as schema_file:
        return json.loads(schema_file.read())
