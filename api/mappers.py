# encoding: utf-8
"""Data Mappers."""
import logging
import motor.motor_asyncio
from tornado.options import options

# pylint: disable=attribute-defined-outside-init
# pylint: disable=no-member

# used because in my opinion there is no reason to reasign
# members from the base class


class UserMapper():
    """User Mapper."""
    def __init__(self,
                 db_client=None):
        """Constructor."""
        print(options)
        if db_client is None:
            db_client = motor.motor_asyncio.AsyncIOMotorClient(host=options.mongo_db_host,
                                                               port=int(options.mongo_db_port))
        self.db_id = None
        self.client = db_client.local.users

    def to_document(self):
        """Parse Object to mongodb document."""
        document = {"email": self.email,
                    "password": self.password,
                    "city": self.city}
        return document

    def to_response(self):
        """Parse Document to response."""
        document = {"email": self.email,
                    "city": self.city}
        return document

    async def find(self):
        """Find the user in the db."""
        document = {'email': self.email}
        result = await self.client.find_one(document)
        logging.debug(result)
        if result is None:
            logging.debug("could not find user %s in db", self.email)
            return False
        self.password = result['password']
        self.db_id = result['_id']
        return self

    async def insert(self):
        """Insert into the db."""
        document = self.to_document()
        result = await self.client.insert_one(document)
        self.db_id = result.inserted_id
        return result.inserted_id

    async def update(self):
        """Update the db."""
        document = self.to_document()
        await self.client.replace_one({'_id': self.db_id}, document)

    async def delete(self):
        """Delete the object in the db."""
        await self.client.delete_many({'_id': self.db_id})
