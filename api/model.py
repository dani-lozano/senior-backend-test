"""Models to be used on autotagging."""
import logging
from werkzeug.security import generate_password_hash, check_password_hash

from mappers import UserMapper


class PasswordEncrypt():
    """Password Encrypt."""

    @classmethod
    def encrypt(cls,
                raw_password: str):
        """Encrypt the password."""
        password = generate_password_hash(raw_password)
        return password

    @classmethod
    def check_password(cls,
                       hashed_password: str,
                       raw_password: str):
        """Check password."""
        try:
            auth = check_password_hash(hashed_password, raw_password)
        except (ValueError, TypeError) as e:
            logging.exception(e)
            auth = False
        else:
            return auth


class User(UserMapper):
    """User domain object."""
    def __init__(self,
                 email='',
                 password='',
                 city=''):
        """Constructor."""
        self.__email = None
        self.__password = None
        self.__city = None
        self.email = email
        self.city = city
        self.encrypt_password(password)
        super(User, self).__init__()

    @property
    def email(self):
        """Email getter."""
        return self.__email

    @email.setter
    def email(self, email):
        self.__email = email

    @property
    def password(self):
        """Password getter."""
        return self.__password

    @password.setter
    def password(self, password):
        self.__password = password

    @property
    def city(self):
        """City getter."""
        return self.__city

    @city.setter
    def city(self, city):
        self.__city = city

    def encrypt_password(self, password):
        """Raw password encryption."""
        self.password = PasswordEncrypt.encrypt(password)

    def check_password(self, password):
        """Check the password against the user. Assumes true password is loaded."""
        return PasswordEncrypt.check_password(self.password, password)
