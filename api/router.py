# encoding: utf-8
# pylint: disable=abstract-method

"""Template backend api.

The purpose of this module is to validate the input params,
parse and adapt them and send to service.
"""
import argparse
import json
import logging

import tornado
from tornado import web
from tornado.log import enable_pretty_logging
from tornado.options import options as app_options

import config as app_config
from model import User
import assertions.schema_validation as validator

enable_pretty_logging()


class PasswordAuth():
    """Authenticate a User."""

    # pylint: disable=no-member,too-few-public-methods
    async def authenticate(self):
        """Authenticate the API request."""
        data = {}
        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.decoder.JSONDecodeError:
            data = {}

        valid = True
        if 'email' not in data:
            valid = False
        if 'password' not in data:
            valid = False
        if not valid:
            self.send_error(status_code=401)

        db_user = User(email=data['email'],
                       password='')
        logging.debug("db_user pass: %s", db_user.password)
        db_user = await db_user.find()

        if not db_user:
            self.send_error(status_code=401)
            return False

        if db_user.check_password(data['password']):
            self.request.user = db_user
            return True

        logging.debug("Auth failed: %s", db_user.email)
        self.send_error(status_code=401)
        return False


class StatusHandler(web.RequestHandler):
    """API call to display the server status."""
    async def get(self, *args, **kwargs):
        """Public get function."""
        self.write("ok")
        self.finish()


class APIHandler(web.RequestHandler, PasswordAuth):
    """API call for POST verb."""
    async def post(self, *args, **kwargs):
        """POST verb function."""
        try:
            data = tornado.escape.json_decode(self.request.body)
        except json.decoder.JSONDecodeError:
            return self.send_error(status_code=400)

        if not (validator.assert_valid_schema(data, 'user_request.json')):  # see assertions/schemas
            return self.send_error(status_code=400)

        db_user = User(email=data["email"],
                       password=data["password"],
                       city=data["city"])

        user_exists = await db_user.find()
        if user_exists is False:
            await db_user.insert()
            self.set_header("Content-Type", "application/json")
            self.write(db_user.to_response())
            return self.finish()

        return self.send_error(status_code=409, reason="user already exists")


class DebugHandler(web.RequestHandler, PasswordAuth):
    """API call that print the input parameter in the console."""
    async def post(self, *args, **kwargs):
        """Public get function."""
        if not (await self.authenticate()):
            return

        if self.request.user.email != 'admin@test.com':
            logging.error("email is not admin: %s", self.request.user.email)
            self.send_error(status_code=401)
            return

        self.set_header("Content-Type", "application/json")
        options = app_options.as_dict()
        self.write(options)
        self.finish()


def make_app():
    """App object."""
    return tornado.web.Application([
        ("/status", StatusHandler),
        ("/api", APIHandler),
        ("/debug", DebugHandler)])


def main():
    """Main."""
    app = make_app()
    parser = argparse.ArgumentParser()
    parser.add_argument('-p',
                        help="which port to serve content on",
                        type=int,
                        dest='port',
                        default=5000)
    args = parser.parse_args()
    # Options class exists, strange behaviour
    app_config.Options()  # pylint: disable=no-member
    logging.info(args)
    app.listen(args.port)
    logging.info("App listening in port: %s", args.port)
    # try:
    tornado.ioloop.IOLoop.instance().start()
    # except KeyboardInterrupt:
    #     pass


if __name__ == '__main__':
    main()
