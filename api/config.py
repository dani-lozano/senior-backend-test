# encoding: utf-8
"""Configuration."""
import logging
import os
from tornado.options import define

# pylint: disable=too-few-public-methods


class Options():
    """Options.

    Define the configuration options.

    Args:
        environment (str): The default environment to start on. Note that ENV variable will overwrite this.
        apikey (str): The default apikey to start the server. Note that APIKEY variable will overwrite this.
    """

    def __init__(self,
                 environment='development',
                 apikey='development-api-key',
                 mongo_db_host='localhost',
                 mongo_db_port='27017'):
        """Constructor."""
        self.define_option(option_name='env',
                           option_value=environment)
        self.define_option(option_name='apikey',
                           option_value=apikey)
        self.define_option(option_name='mongo_db_host',
                           option_value=mongo_db_host)
        self.define_option(option_name='mongo_db_port',
                           option_value=mongo_db_port)

    @staticmethod
    def define_option(option_name,
                      option_value):
        """Define a configuration option."""
        var_environment = os.environ.get(option_name.upper(), None)
        default_value = option_value
        if var_environment:
            default_value = var_environment
        define(option_name, default=default_value, help="{} option.".format(option_name))
        logging.info("The %s is set to: %s", option_name, default_value)
