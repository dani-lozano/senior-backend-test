# encoding: utf-8
"""API Testing."""
import sys
import logging
import json
import tornado.escape
import tornado.testing
import tornado.ioloop
from tornado.options import options as tornado_options

sys.path.insert(0, 'api/')
import router as api_module  # pylint: disable=wrong-import-position,import-error,
from model import User  # pylint: disable=wrong-import-position,import-error,
from config import Options  # pylint: disable=wrong-import-position,import-error,no-name-in-module
Options(environment='test')


class TestApp(tornado.testing.AsyncHTTPTestCase):
    """Test class. It test API."""
    def setUp(self):
        user = User(email="admin@test.com",
                    password="password",
                    city="Berlin")
        user_not_admin = User(email="not_admin@test.com",
                              password="password",
                              city="Berlin")
        logging.debug("setting up the user for testing: %s", user.password)
        logging.debug("setting up the user_not_admin for testing: %s", user_not_admin.password)
        tornado.ioloop.IOLoop.current().run_sync(user.insert)
        tornado.ioloop.IOLoop.current().run_sync(user_not_admin.insert)
        super(TestApp, self).setUp()

    def tearDown(self):
        user_admin = User(email="admin@test.com")
        user_not_admin = User(email="not_admin@test.com")
        user_inserted = User(email="correct@post.com")
        exist_admin = tornado.ioloop.IOLoop.current().run_sync(user_admin.find)
        exist_not_admin = tornado.ioloop.IOLoop.current().run_sync(user_not_admin.find)
        exist_inserted = tornado.ioloop.IOLoop.current().run_sync(user_inserted.find)

        if exist_admin:
            tornado.ioloop.IOLoop.current().run_sync(user_admin.delete)

        if exist_not_admin:
            tornado.ioloop.IOLoop.current().run_sync(user_not_admin.delete)

        if exist_inserted:
            tornado.ioloop.IOLoop.current().run_sync(user_inserted.delete)

    def get_app(self):
        return api_module.make_app()

    def test_options(self):
        """Test debug method POST."""
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 200)
        response_body = tornado.escape.json_decode(response.body)
        logging.debug(response_body)
        self.assertEqual({"help": None,
                          "logging": "info",
                          "log_to_stderr": None,
                          "log_file_prefix": None,
                          "log_file_max_size": 100000000,
                          "log_file_num_backups": 10,
                          "log_rotate_when": "midnight",
                          "log_rotate_interval": 1,
                          "log_rotate_mode": "size",
                          "env": "test",
                          "apikey": "development-api-key",
                          "mongo_db_host": tornado_options.mongo_db_host,
                          "mongo_db_port": tornado_options.mongo_db_port},
                         response_body)

    def test_options_auth(self):
        """Test debug method POST."""

        # 401 without email
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"password": "password"}))
        self.assertEqual(response.code, 401)

        # 401 without password
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "email"}))
        self.assertEqual(response.code, 401)

        # 401 without not json
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body="not JSON object")

        # Admin is the only used authorized
        # not_admin is a valid user in our db
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "not_admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 401)

        # incorrect password
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "245"}))
        self.assertEqual(response.code, 401)

        # correct call
        response = self.fetch('/debug',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 200)

    def test_status(self):
        """Test status method GET."""
        response = self.fetch('/status',
                              method="GET")
        self.assertEqual(response.code, 200)
        self.assertEqual(response.body.decode('utf-8'), 'ok')

    def test_api_post(self):
        """Test API method POST."""

        # JSON body is a must)
        response = self.fetch('/api',
                              method="POST",
                              headers={},
                              body="Not JSON")
        self.assertEqual(response.code, 400)

        # Can't be not well formed request (must to be email, password and city)
        response = self.fetch('/api',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password"}))
        self.assertEqual(response.code, 400)

        # Can't be a city not in  "Berlin", "New York", "Helsinki", "Rome", "Budapest", "New Delhi"
        response = self.fetch('/api',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "admin@test.com",
                                               "password": "password",
                                               "city": "Barcelona"}))
        self.assertEqual(response.code, 400)

        # Correct post
        response = self.fetch('/api',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "correct@post.com",
                                               "password": "password",
                                               "city": "Berlin"}))
        self.assertEqual(response.code, 200)

        # Can't post a user that already exists (email)
        response = self.fetch('/api',
                              method="POST",
                              headers={},
                              body=json.dumps({"email": "correct@post.com",
                                               "password": "pass",
                                               "city": "Budapest"}))
        self.assertEqual(response.code, 409)
