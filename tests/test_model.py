# encoding: utf-8
"""User model test."""
import sys
import pytest
sys.path.insert(0, 'api/')
from model import User, PasswordEncrypt  # pylint: disable=wrong-import-position,import-error


class TestUser():
    """User test class. It tests User model."""
    @pytest.mark.asyncio
    async def test_user(self):
        """Test User model."""

        # Test insertion.
        res = User(email='test@wide-eyes.it',
                   password='password')
        user = res
        uid = await user.insert()
        assert uid is not None

        # Test update.
        user.password = '134256'
        await user.update()

        # Test correct password setter.
        await user.find()
        user_diff_password = User(email=user.email,
                                  password='1623')
        assert user.password != user_diff_password.password

    @pytest.mark.asyncio
    async def test_auth(self):
        "Test correct password setting."
        user = User(email="notest@foo.com",
                    password="744")
        user_diff_password = User(email='test@test.com',
                                  password='1623')
        assert user.password != user_diff_password.password
        assert user.check_password(password='744')

    @staticmethod
    def test_encrypt():
        "Test correct encryption."
        assert PasswordEncrypt.encrypt(raw_password='1623') != PasswordEncrypt.encrypt(raw_password='744')
        salted_password = PasswordEncrypt.encrypt(raw_password='1623')
        assert PasswordEncrypt.check_password(hashed_password=salted_password,
                                              raw_password='1623')
