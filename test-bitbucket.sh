#! /bin/bash
for file in *; do
    if [ -d $file ] && [ $file != "build" ] && [ $file != "dist" ] && [ $file != "docs" ] && [ $file != *"egg-info"* ] && [ $file != "test" ]; then
	pylint $file -f parseable -r n --disable=superfluous-parens,invalid-name --max-line-length=120
	pycodestyle $file --max-line-length=120 --ignore=E402
	pydocstyle $file --ignore=D204,D213,D406,D407,D203
    fi
done
 pytest --cov=.
